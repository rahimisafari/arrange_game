<?php

defined('_JEXEC') or die;
require_once dirname(__FILE__) . '/helper.php';

JHtml::script('mod_arrange_game/arrange.js', false, true);
JHtml::stylesheet('mod_arrange_game/arrange.css',array(), true);
$options=$params->toObject();
require JModuleHelper::getLayoutPath('mod_arrange_game');

