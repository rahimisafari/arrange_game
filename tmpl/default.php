<?php
defined('_JEXEC') or die;?>

<script type="text/javascript">
	jQuery(document).ready(function (e) {
		toggleHelp();
		loadBoard(4);
	});
</script>

<br />

<div>
	<p style="font-weight:bold"><?php echo $options->helptitle;?></p>
	<p>
		<input type=button id=butHelp value="مخفی کردن راهنمای بازی" class="but" onclick="toggleHelp();_gaq.push(['_trackEvent','Arrange Game','Discription','no value']);">
		<span id="help" class="help"><?php echo  $options->helptext; ?></span></p>
	
</div>

<div>
	<input type=button class=but value="شروع بازی"
		onclick="startGame(); _gaq.push(['_trackEvent','Arrange Game','Started','no value']);">
</div>

<div style="text-align:center">
	<div id="board"></div>
	<div id="fldStatus"></div>
</div>