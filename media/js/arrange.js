var gsize, ghrow, ghcol, gtime, gmoves, gintervalid=-1, gshuffling;

function toggleHelp()
{
  if (butHelp.value == "مخفی کردن راهنمای بازی")
  {
    help.style.display = "none";
    butHelp.value = "نمایش راهنمای بازی";
  }
  else
  {
    help.style.display = "";
    butHelp.value = "مخفی کردن راهنمای بازی";
  }  
}

//random number between low and hi
function r(low,hi)
{
  return Math.floor((hi-low)*Math.random()+low); 
}

//random number between 1 and hi
function r1(hi)
{
  return Math.floor((hi-1)*Math.random()+1); 
}

//random number between 0 and hi
function r0(hi)
{
  return Math.floor((hi)*Math.random()); 
}

var startTime;
function startGame()
{
  shuffle();
  gtime = 0;
  gmoves = 0;
  clearInterval(gintervalid);
  tickTime();
  gintervalid = setInterval("tickTime()",1000);

  //calculate game play time by rahimi-------------
  var dateNow=new Date();
  startTime=dateNow.getHours()*60+dateNow.getMinutes()*60+dateNow.getSeconds();
}

function stopGame()
{
  if (gintervalid==-1) return;
  clearInterval(gintervalid);
  fldStatus.innerHTML = "";
  gintervalid=-1;
}

function tickTime()
{
  showStatus();
  gtime++;
}

function checkWin()
{
  // s = "<table cellpadding=4>";
  // s += "<tr><td align=center class=capt3>!! تبریک می گوییم !!</td></tr>";
  // s += "<tr class=capt4><td align=center>شما بازی را در " + gtime + " ثانیه ";
  // s += "با  " + gmoves + " تعداد حرکت به پایان رسانده و موفق شدید خدمات لوله بازکنی ما را به صورت صد در صد رایگان کسب کنید!</td></tr>";
  // s += "<tr><td align=center class=capt4>سرعت شما " + Math.round(1000*gmoves/gtime)/1000 + " حرکت در ثانیه</td></tr>";
  // s += "<tr><td class=capt3><a onClick='takhf(100)'> لطفا برای دریافت کد تخفیف و ارائه آن به سرویسکار اینجا کلیک کنید </a></td></tr>";
  // s += "</table>";
  // fldStatus.innerHTML = s;
  var i, j, s;
  
  if (gintervalid==-1) return; //game not started!
  
  if (!isHole(gsize-1,gsize-1)) return;
  
//  checkHalf=0;
  
  for (i=0;i<gsize;i++)
    for (j=0;j<gsize;j++)
    {
      if (!(i==gsize-1 && j==gsize-1)) //ignore last block (ideally a hole)
      {
		  /*if(checkHalf==6)
		  {
			  alert("is half");
			  }*/
       if (getValue(i,j)!=(i*gsize+j+1).toString()) return;
	   //checkHalf++;
      }
    }
  //console.log('stoping');
  stopGame();

  /*s = "<table cellpadding=4>";
  s += "<tr><td align=center class=capt3>!! تبریک می گوییم !!</td></tr>";
  s += "<tr class=capt4><td align=center>شما بازی را در " + gtime + " ثانیه ";
  s += "با  " + gmoves + " تعداد حرکت به پایان رسانده و موفق شدید خدمات لوله بازکنی 15 هزار تومانی ما را کسب کنید!</td></tr>";
  s += "<tr><td align=center class=capt4>سرعت شما " + Math.round(1000*gmoves/gtime)/1000 + " حرکت در ثانیه</td></tr>";
  s += "<tr><td class=capt3><a onClick='takhf(100)'> لطفا برای دریافت کد تخفیف و ارائه آن به سرویسکار اینجا کلیک کنید </a></td></tr>";
  s += "</table>";*/

  s = "<br/>";
  s += "<p style='color:red'>!! تبریک می گوییم !!</p>";
  s += "<p>شما بازی را در " + gtime + " ثانیه ";
  s += "با  " + gmoves + " تعداد حرکت به پایان رسانده و موفق شدید کد تخفیف ما را کسب کنید!</p>";
  s += "<p>سرعت شما " + Math.round(1000*gmoves/gtime)/1000 + " حرکت در ثانیه</p>";
  s+="<p>لطفا برای تماس با سرویسکار ، روی دایره سبز رنگ تلفن در پایین سایت کلیک کنید</p>";
  s+="<p>یا اینکه با شماره سرویسکار  09129615767 بگیرید</p>";
  s+="<p>کد تخفیف 55 درصدی شما 8528524 است. پس از انجام کار توسط سرویسکار ، این کد را ارائه داده و 55 درصد کمتر پرداخت کنید</p>";
  //s += "<p><a onClick='takhf(100)'> لطفا برای دریافت کد تخفیف و ارائه آن به سرویسکار اینجا کلیک کنید </a></p>";
  

  //calculate game play time by rahimi------------------------------
  var dateNow2=new Date();
  var endTime=dateNow2.getHours()*60+dateNow2.getMinutes()*60+dateNow2.getSeconds();
  
  var gamePlayTime=endTime-startTime;
  _gaq.push(['_trackEvent','Arrange Game','Wined in secounds',gamePlayTime+ ' secounds']);
  fldStatus.innerHTML = s;
  board.innerHTML='';
//  shuffle();
}

function takhf(percept){
	var telPage=window.open('http://لوله-بازکنی.com/شماره-تلفن-لوله-بازکنی-قم#vkjuyhgtgdjkmlko','_self');
	telPage.focus();
}

function showStatus()
{
  // fldStatus.innerHTML = "Time:&nbsp;" + gtime + " secs&nbsp;&nbsp;&nbsp;Moves:&nbsp;" + gmoves
}

function showTable()
{
  var i, j, s;
  
  stopGame();
  s = "<table border=3 cellpadding=0 cellspacing=0 bgcolor='#666655'><tr><td class=bigcell>";
  s = s + "<table border=0 cellpadding=0 cellspacing=0>";
  for (i=0; i<gsize; i++)
  {
    s = s + "<tr>";    
    for (j=0; j<gsize; j++)
    {
      s = s + "<td id=a_" + i + "_" + j + " onclick='movecell(this)' class=cell>" + (i*gsize+j+1) + "</td>";
    }
    s = s + "</tr>";        
  }
  s = s + "</table>";
  s = s + "</td></tr></table>";      
  return s;
}

function getCell(row, col)
{
  return eval("a_" + row + "_" + col);
}

function setValue(row,col,val)
{
  var v = getCell(row, col);
  v.innerHTML = val;
  v.className = "cell";
}

function getValue(row,col)
{
//  alert(row + "," + col);

  var v = getCell(row, col);
  return v.innerHTML;
}

function setHole(row,col)
{ 
  var v = getCell(row, col);
  v.innerHTML = "";
  v.className = "hole";
  ghrow = row;
  ghcol = col;
}

function getRow(obj)
{
  var a = obj.id.split("_");
  return a[1];
}

function getCol(obj)
{
  var a = obj.id.split("_");
  return a[2];
}

function isHole(row, col)
{
  return (row==ghrow && col==ghcol) ? true : false;
}

function getHoleInRow(row)
{
  var i;
  
  return (row==ghrow) ? ghcol : -1;
}

function getHoleInCol(col)
{
  var i;

  return (col==ghcol) ? ghrow : -1;
}

function shiftHoleRow(src,dest,row)
{
  var i;

  //conversion to integer needed in some cases!
  src = parseInt(src);
  dest = parseInt(dest);

  if (src < dest)
  {
    for (i=src;i<dest;i++)
    {
      setValue(row,i,getValue(row,i+1));
      setHole(row,i+1);
    }
  }
  if (dest < src)
  {
    for (i=src;i>dest;i--)
    {
      setValue(row,i,getValue(row,i-1));
      setHole(row,i-1);
    }
  }
}

function shiftHoleCol(src,dest,col)
{
  var i;
  
  //conversion to integer needed in some cases!
  src = parseInt(src);
  dest = parseInt(dest);
    
  if (src < dest)
  {//alert("src=" + src +" dest=" + dest + " col=" + col);
    for (i=src;i<dest;i++)
    {//alert(parseInt(i)+1);
      setValue(i,col,getValue(i+1,col));
      setHole(i+1,col);
    }
  }
  if (dest < src)
  {
    for (i=src;i>dest;i--)
    {
      setValue(i,col,getValue(i-1,col));
      setHole(i-1,col);
    }
  }
}

function movecell(obj)
{
  var r, c, hr, hc;
  if (gintervalid==-1 && !gshuffling) 
  {
    alert('لطفا برای شروع بازی بر روی دکمه شروع بازی کلیک کنید.با تشکر ');
    return;
  }
  r = getRow(obj);
  c = getCol(obj);
  if (isHole(r,c)) return;
  
  hc = getHoleInRow(r);
  if (hc != -1)
  {
    shiftHoleRow(hc,c,r);
    gmoves++;
    checkWin();
    return;
  }
  
  hr = getHoleInCol(c);

  if (hr != -1)
  {
    shiftHoleCol(hr,r,c);
    gmoves++;
    checkWin();
    return;
  }
}

function shuffle()
{
  var t,i,j,s,frac;

  gshuffling =  true;
  frac = 100.0/(gsize*(gsize+10));
  s = "% ";
  for (i=0;i<gsize;i++)
  {
    s += "|";
    for (j=0;j<gsize+10;j++)
    {  
      window.status = "Loading " + Math.round((i*(gsize+10) + j)*frac) + s  
      if (j%2==0)
      {
        t = r0(gsize);
        while (t == ghrow) t = r0(gsize); //skip holes
        getCell(t,ghcol).click();
      } 
      else
      {
        t = r0(gsize);
        while (t == ghcol) t = r0(gsize); //skip holes
        getCell(ghrow,t).click();
      }
    }
  }
  window.status = "";
  gshuffling = false;
}

function loadBoard(size)
{
  gsize = size;
  
  board.innerHTML = showTable(gsize);
  setHole(gsize-1,gsize-1);
  //shuffle();
}